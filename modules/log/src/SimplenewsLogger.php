<?php

namespace Drupal\contacts_log;

use Drupal\simplenews\SubscriberInterface;

/**
 * Service to handle logging for simplenews changes.
 */
class SimplenewsLogger extends DestructableLoggerBase {

  /**
   * React to a subscriber being saved.
   *
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The subscriber entity.
   * @param \Drupal\simplenews\SubscriberInterface|null $original
   *   The original subscriber entity, if there was one.
   */
  public function postSave(SubscriberInterface $subscriber, ?SubscriberInterface $original = NULL): void {
    $this->handleSubscriptionChanges($subscriber, $original);
    $this->handleSubscriberDisabled($subscriber, $original);
  }

  /**
   * React to a subscriber being removed.
   *
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The subscriber entity.
   */
  public function postDelete(SubscriberInterface $subscriber): void {
    // Check the template early to avoid unnecessary processing.
    if (!$this->hasTemplate('contacts_log_simplenews_removed')) {
      return;
    }

    $unsubscribed_reason = $this->getUnsubscribeReason($subscriber);

    $this->log((string) $subscriber->id(), [
      'template' => 'contacts_log_simplenews_removed',
      'contact' => $subscriber->getUserId(),
      'unsubscribed_reason' => $unsubscribed_reason,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function mergeLog(array $existing, array $new): array {
    // Remove the originals to preserve the original from the first instance.
    unset($new['subscriptions_original']);

    // Merge, preferring values on the newer item.
    return $new + $existing;
  }

  /**
   * {@inheritdoc}
   */
  protected function createMessage($update): void {
    if ($update['template'] === 'contacts_log_simplenews') {
      // Only create if the subscriptions have changed or
      // subscriber has been removed.
      $original_ids = $update['subscriptions_original'] ?? [];
      sort($original_ids);
      $new_ids = $update['subscriptions'] ?? [];
      sort($new_ids);

      if ($original_ids == $new_ids) {
        return;
      }
    }

    parent::createMessage($update);
  }

  /**
   * Handle changes to subscriptions message.
   *
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The subscriber entity.
   * @param \Drupal\simplenews\SubscriberInterface|null $original
   *   The original unchanged subscriber.
   */
  private function handleSubscriptionChanges(SubscriberInterface $subscriber, ?SubscriberInterface $original = NULL): void {
    // Check the template early to avoid unnecessary processing.
    if (!$this->hasTemplate('contacts_log_simplenews')) {
      return;
    }

    $this->log((string) $subscriber->id(), [
      'template' => 'contacts_log_simplenews',
      'contact' => $subscriber->getUserId(),
      'subscriptions' => $subscriber->getSubscribedNewsletterIds(),
      'subscriptions_original' => $original ? $original->getSubscribedNewsletterIds() : [],
    ]);
  }

  /**
   * Handle disable message for subscriber.
   *
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The subscriber entity.
   * @param \Drupal\simplenews\SubscriberInterface|null $original
   *   The original unchanged subscriber.
   */
  private function handleSubscriberDisabled(SubscriberInterface $subscriber, ?SubscriberInterface $original = NULL): void {
    // Check the template early to avoid unnecessary processing.
    if (!$this->hasTemplate('contacts_log_simplenews_disabled')) {
      return;
    }

    // Needs to be a change to disabled.
    if (!$original) {
      return;
    }

    // Subscriber is not disabled.
    if ($subscriber->getStatus() != SubscriberInterface::INACTIVE) {
      return;
    }

    // Subscriber was already disabled.
    if ($original->getStatus() == SubscriberInterface::INACTIVE) {
      return;
    }

    $unsubscribed_reason = $this->getUnsubscribeReason($subscriber);
    $this->log((string) $subscriber->id(), [
      'template' => 'contacts_log_simplenews_disabled',
      'contact' => $subscriber->getUserId(),
      'unsubscribed_reason' => $unsubscribed_reason,
    ]);
  }

  /**
   * Get the unsubscribe reason from the subscriber.
   *
   * @param \Drupal\simplenews\SubscriberInterface $subscriber
   *   The subscriber.
   *
   * @return string
   *   The reason if there is one.
   */
  private function getUnsubscribeReason(SubscriberInterface $subscriber): string {
    $unsubscribed_reason = 'unknown';
    if ($subscriber->hasField('unsubscribed_reason') && !$subscriber->get('unsubscribed_reason')->isEmpty()) {
      $unsubscribed_reason = $subscriber->get('unsubscribed_reason')->value;
    }

    return $unsubscribed_reason;
  }

}
