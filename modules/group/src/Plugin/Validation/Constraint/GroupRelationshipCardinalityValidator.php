<?php

namespace Drupal\contacts_group\Plugin\Validation\Constraint;

use Drupal\group\Plugin\Validation\Constraint\GroupRelationshipCardinalityValidator as GroupContraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * {@inheritdoc}
 */
class GroupRelationshipCardinalityValidator extends GroupContraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($group_relationship, Constraint $constraint): void {
    /** @var \Drupal\group\Entity\GroupRelationshipInterface $group_relationship */
    // Exit early if the group is unsaved.
    if ($group = $group_relationship->getGroup()) {
      if ($group->isNew()) {
        return;
      }
    }

    parent::validate($group_relationship, $constraint);
  }

}
