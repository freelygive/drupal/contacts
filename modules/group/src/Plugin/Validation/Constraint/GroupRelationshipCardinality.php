<?php

namespace Drupal\contacts_group\Plugin\Validation\Constraint;

use Drupal\group\Plugin\Validation\Constraint\GroupRelationshipCardinality as GroupConstraint;

/**
 * {@inheritdoc}
 */
class GroupRelationshipCardinality extends GroupConstraint {
}
