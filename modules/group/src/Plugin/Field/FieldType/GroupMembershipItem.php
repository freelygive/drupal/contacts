<?php

namespace Drupal\contacts_group\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'group_membership' field type.
 *
 * @FieldType(
 *   id = "group_membership",
 *   label = @Translation("Group membership"),
 *   description = @Translation("Field item for computed group membership fields"),
 *   no_ui = TRUE,
 *   list_class = "\Drupal\contacts_group\Plugin\Field\GroupMembershipItemList",
 * )
 *
 * @property \Drupal\group\Entity\GroupRelationship $membership
 * @property \Drupal\group\Entity\GroupRelationship $entity
 */
class GroupMembershipItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    // Backwards compatibility: Expose the underlying GroupRelationship entity
    // as a property called 'membership' in addition to the underlying 'entity'
    // property. This is done because for group 1.x we exposed a Membership
    // wrapper on this property, which has subsequently been deprecated in
    // group 2.x onwards. The methods from this wrapper were moved to the entity
    // so we just expose the entity directly here for compatibility with callers
    // who might be trying to access the membership property.
    $properties['membership'] = DataDefinition::create('any')
      ->setLabel(new TranslatableMarkup('Membership'))
      ->setDescription(new TranslatableMarkup('Underlying group relationship entity'));

    return $properties;
  }

}
